$(document).ready(() => {
  const trelloCards = [];
  const key = 'key=5ebb2e3505df87a317f75b76d08add31';
  const token = 'token=4a18f0fc020cd3aee8d60deacfd95be1b6bfcfe10bd6f7cd58a7139f9a2eefb3';
  const getCardURL = `https://api.trello.com/1/boards/5b8e1d2cd80f433972547169/cards/?limit=2&fields=name&members=true&member_fields=fullName&${key}&${token}`;
  fetch(getCardURL)
    .then(res => res.json())
    .then((data) => {
      data.map((cardIDs) => {
        trelloCards.push(cardIDs.id);
        return trelloCards;
      });
      trelloCards.forEach((trelloCard) => {
        const getURL = `https://api.trello.com/1/cards/${trelloCard}/checklists?${key}&${token}`;
        fetch(getURL)
          .then(res => res.json())
          .then((dataCheckList) => {
            dataCheckList.forEach((checklists) => {
              checklists.checkItems.forEach((checklistNames) => {
                if (checklistNames.state === 'incomplete') {
                  $('#form-undone-tasks').append(`<label><input value="${checklistNames.id}"  class="cb pristine" type="checkbox"> <span>${checklistNames.name}</span></label>`);
                }
                if (checklistNames.state === 'complete') {
                  $('#form-undone-tasks').append(`<label><input value="${checklistNames.id}" class="cb pristine" type="checkbox" checked> <span><strike>${checklistNames.name}</strike></span></label>`);
                }
                const selector = document.querySelectorAll('.cb');
                for (const i in selector) {
                  if (i < selector.length) {
                    selector[i].addEventListener('click', function checkboxCode() {
                      const c = this.classList;
                      const p = 'pristine';
                      if (c.contains(p)) {
                        c.remove(p);
                      }
                    });
                  }
                }
              });
            });
            const checkboxes = $('input[type="checkbox"]');
            checkboxes.change(function putRequest() {
              const status = (this.checked) ? 'state=complete' : 'state=incomplete';
              const putURL = `https://api.trello.com/1/cards/${trelloCard}/checkItem/${this.value}/?${status}&${key}&${token}`;
              fetch(putURL, {
                method: 'PUT',
              });
            });
          });
      });
    });
});
